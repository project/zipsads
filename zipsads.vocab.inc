<?php

/**
 * @file
 *   Vocab functions for the zipsads module.
 * @ingroup zipsads
 */

/**
 * zipsads_save_term.
 */
function zipsads_save_term($name, $description, $vid, $weight, $parent) {
  $term = array(
    'name' => $name,
    'description' => $description,
    'vid' => $vid,
    'weight' => $weight,
    'parent' => $parent,
  );
  taxonomy_save_term($term);
}

/**
 * zipsads_categories_vocabulary_vid.
 */
function zipsads_categories_vocabulary_vid() {
  $vid = variable_get('zipsads_categories_vocabulary', '');
  if (empty($vid)) {
    $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `module` = 'zipsads'"));
    if (!$vid) {
      $vocabulary = array(
        'name' => t('Zipsads categories'),
        'multiple' => 0,
        'required' => 1,
        'hierarchy' => 1,
        'relations' => 0,
        'module' => 'zipsads',
        'nodes' => array('zipsads_category' => 1),
      );
      taxonomy_save_vocabulary($vocabulary);
      $vid = $vocabulary['vid'];

      zipsads_save_term('community', 'community heading', $vid, -90, 0);
      $tid = db_result(db_query("SELECT `tid` FROM {term_data} WHERE `name` = 'community'"));
      zipsads_save_term('activities', '', $vid, -98, $tid);
      zipsads_save_term('artists', '', $vid, -96, $tid);
      zipsads_save_term('childcare', '', $vid, -94, $tid);
      zipsads_save_term('general', '', $vid, -92, $tid);
      zipsads_save_term('groups', '', $vid, -90, $tid);
      zipsads_save_term('pets', '', $vid, -88, $tid);
      zipsads_save_term('events', '', $vid, -86, $tid);
      zipsads_save_term('lost+found', '', $vid, -84, $tid);
      zipsads_save_term('musicians', '', $vid, -82, $tid);
      zipsads_save_term('local news', '', $vid, -80, $tid);
      zipsads_save_term('politics', '', $vid, -78, $tid);
      zipsads_save_term('rideshare', '', $vid, -76, $tid);
      zipsads_save_term('volunteers', '', $vid, -74, $tid);
      zipsads_save_term('classes', '', $vid, -72, $tid);
      zipsads_save_term('(other community)', '', $vid, -70, $tid);

      zipsads_save_term('personals', '', $vid, -80, 0);
      $tid = db_result(db_query("SELECT `tid` FROM {term_data} WHERE `name` = 'personals'"));
      zipsads_save_term('strictly platonic', '', $vid, -98, $tid);
      zipsads_save_term('women seeking women', '', $vid, -96, $tid);
      zipsads_save_term('women seeking men', '', $vid, -94, $tid);
      zipsads_save_term('men seeking women', '', $vid, -92, $tid);
      zipsads_save_term('men seeking men', '', $vid, -90, $tid);
      zipsads_save_term('misc romance', '', $vid, -88, $tid);
      zipsads_save_term('casual encounters', '', $vid, -86, $tid);
      zipsads_save_term('missed connections', '', $vid, -84, $tid);
      zipsads_save_term('rants and raves', '', $vid, -82, $tid);
      zipsads_save_term('(other personals)', '', $vid, -80, $tid);

      zipsads_save_term('housing', '', $vid, -70, 0);
      $tid = db_result(db_query("SELECT `tid` FROM {term_data} WHERE `name` = 'housing'"));
      zipsads_save_term('apts / housing', '', $vid, -98, $tid);
      zipsads_save_term('rooms / shared', '', $vid, -96, $tid);
      zipsads_save_term('sublets / temporary', '', $vid, -94, $tid);
      zipsads_save_term('housing wanted', '', $vid, -92, $tid);
      zipsads_save_term('housing swap', '', $vid, -90, $tid);
      zipsads_save_term('vacation rentals', '', $vid, -88, $tid);
      zipsads_save_term('parking / storage', '', $vid, -86, $tid);
      zipsads_save_term('office / commercial', '', $vid, -84, $tid);
      zipsads_save_term('real estate for sale', '', $vid, -82, $tid);
      zipsads_save_term('(other housing)', '', $vid, -80, $tid);

      zipsads_save_term('for sale', '', $vid, -60, 0);
      $tid = db_result(db_query("SELECT `tid` FROM {term_data} WHERE `name` = 'for sale'"));
      zipsads_save_term('arts+crafts', '', $vid, -98, $tid);
      zipsads_save_term('auto parts', '', $vid, -96, $tid);
      zipsads_save_term('baby+kids', '', $vid, -94, $tid);
      zipsads_save_term('barter', '', $vid, -92, $tid);
      zipsads_save_term('bikes', '', $vid, -90, $tid);
      zipsads_save_term('boats', '', $vid, -88, $tid);
      zipsads_save_term('books', '', $vid, -86, $tid);
      zipsads_save_term('business', '', $vid, -84, $tid);
      zipsads_save_term('cars+trucks', '', $vid, -82, $tid);
      zipsads_save_term('cds/dvd/vhs', '', $vid, -80, $tid);
      zipsads_save_term('clothes+acc', '', $vid, -78, $tid);
      zipsads_save_term('collectibles', '', $vid, -76, $tid);
      zipsads_save_term('computer', '', $vid, -74, $tid);
      zipsads_save_term('electronics', '', $vid, -72, $tid);
      zipsads_save_term('farm+garden', '', $vid, -70, $tid);
      zipsads_save_term('free', '', $vid, -68, $tid);
      zipsads_save_term('furniture', '', $vid, -66, $tid);
      zipsads_save_term('games+toys', '', $vid, -64, $tid);
      zipsads_save_term('garage sale', '', $vid, -62, $tid);
      zipsads_save_term('general', '', $vid, -60, $tid);
      zipsads_save_term('household', '', $vid, -58, $tid);
      zipsads_save_term('jewelry', '', $vid, -56, $tid);
      zipsads_save_term('material', '', $vid, -54, $tid);
      zipsads_save_term('motorcycles', '', $vid, -52, $tid);
      zipsads_save_term('music instr', '', $vid, -50, $tid);
      zipsads_save_term('photo+video', '', $vid, -48, $tid);
      zipsads_save_term('rvs', '', $vid, -46, $tid);
      zipsads_save_term('sporting', '', $vid, -44, $tid);
      zipsads_save_term('tickets', '', $vid, -42, $tid);
      zipsads_save_term('tools', '', $vid, -40, $tid);
      zipsads_save_term('wanted', '', $vid, -38, $tid);
      zipsads_save_term('(other for sale)', '', $vid, -36, $tid);

      zipsads_save_term('services', '', $vid, -50, 0);
      $tid = db_result(db_query("SELECT `tid` FROM {term_data} WHERE `name` = 'services'"));
      zipsads_save_term('automotive', '', $vid, -98, $tid);
      zipsads_save_term('beauty', '', $vid, -96, $tid);
      zipsads_save_term('computer', '', $vid, -94, $tid);
      zipsads_save_term('creative', '', $vid, -92, $tid);
      zipsads_save_term('erotic', '', $vid, -90, $tid);
      zipsads_save_term('event', '', $vid, -88, $tid);
      zipsads_save_term('financial', '', $vid, -86, $tid);
      zipsads_save_term('household', '', $vid, -84, $tid);
      zipsads_save_term('labor/move', '', $vid, -82, $tid);
      zipsads_save_term('legal', '', $vid, -80, $tid);
      zipsads_save_term('lessons', '', $vid, -78, $tid);
      zipsads_save_term('skilled trade', '', $vid, -76, $tid);
      zipsads_save_term('real estate', '', $vid, -74, $tid);
      zipsads_save_term('sm biz ads', '', $vid, -72, $tid);
      zipsads_save_term('therapeutic', '', $vid, -70, $tid);
      zipsads_save_term('travel/vac', '', $vid, -68, $tid);
      zipsads_save_term('write/ed/tr8', '', $vid, -66, $tid);
      zipsads_save_term('(other services)', '', $vid, -64, $tid);

      zipsads_save_term('jobs', '', $vid, -40, 0);
      $tid = db_result(db_query("SELECT `tid` FROM {term_data} WHERE `name` = 'jobs'"));
      zipsads_save_term('accounting+finance', '', $vid, -98, $tid);
      zipsads_save_term('admin / office', '', $vid, -96, $tid);
      zipsads_save_term('arch / engineering', '', $vid, -94, $tid);
      zipsads_save_term('art / media / design', '', $vid, -92, $tid);
      zipsads_save_term('biotech / science', '', $vid, -90, $tid);
      zipsads_save_term('business / mgmt', '', $vid, -88, $tid);
      zipsads_save_term('customer service', '', $vid, -86, $tid);
      zipsads_save_term('education', '', $vid, -84, $tid);
      zipsads_save_term('food / bev / hosp', '', $vid, -82, $tid);
      zipsads_save_term('general labor', '', $vid, -80, $tid);
      zipsads_save_term('government', '', $vid, -78, $tid);
      zipsads_save_term('human resources', '', $vid, -76, $tid);
      zipsads_save_term('internet engineers', '', $vid, -74, $tid);
      zipsads_save_term('legal / paralegal', '', $vid, -72, $tid);
      zipsads_save_term('manufacturing', '', $vid, -70, $tid);
      zipsads_save_term('marketing / pr / ad', '', $vid, -68, $tid);
      zipsads_save_term('medical / health', '', $vid, -66, $tid);
      zipsads_save_term('nonprofit sector', '', $vid, -64, $tid);
      zipsads_save_term('real estate', '', $vid, -62, $tid);
      zipsads_save_term('retail / wholesale', '', $vid, -60, $tid);
      zipsads_save_term('sales / biz dev', '', $vid, -58, $tid);
      zipsads_save_term('salon / spa / fitness', '', $vid, -56, $tid);
      zipsads_save_term('security', '', $vid, -54, $tid);
      zipsads_save_term('skilled trade / craft', '', $vid, -52, $tid);
      zipsads_save_term('software / qa / dba', '', $vid, -50, $tid);
      zipsads_save_term('systems / network', '', $vid, -48, $tid);
      zipsads_save_term('technical support', '', $vid, -46, $tid);
      zipsads_save_term('transport', '', $vid, -44, $tid);
      zipsads_save_term('tv / film / video', '', $vid, -42, $tid);
      zipsads_save_term('web / info design', '', $vid, -40, $tid);
      zipsads_save_term('writing / editing', '', $vid, -38, $tid);
      zipsads_save_term('(part time)', '', $vid, -36, $tid);
      zipsads_save_term('(other jobs)', '', $vid, -34, $tid);

      zipsads_save_term('gigs', '', $vid, -30, 0);
      $tid = db_result(db_query("SELECT `tid` FROM {term_data} WHERE `name` = 'gigs'"));
      zipsads_save_term('adult', '', $vid, -98, $tid);
      zipsads_save_term('computer', '', $vid, -96, $tid);
      zipsads_save_term('creative', '', $vid, -94, $tid);
      zipsads_save_term('crew', '', $vid, -92, $tid);
      zipsads_save_term('domestic', '', $vid, -90, $tid);
      zipsads_save_term('event', '', $vid, -88, $tid);
      zipsads_save_term('labor', '', $vid, -86, $tid);
      zipsads_save_term('talent', '', $vid, -84, $tid);
      zipsads_save_term('writing', '', $vid, -82, $tid);
      zipsads_save_term('(other gigs)', '', $vid, -80, $tid);

    }
    variable_set('zipsads_categories_vocabulary', $vid);
  }
  return $vid;
}

