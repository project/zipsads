<?php

/**
 * @file
 *   Main file for the zipsads module for drupal 5.
 *
 *   A user selects a location and a category, and can then view the ad listings for those settings.
 *   A registered user can place ads, and then manage (edit, delete, renew) his ads.
 *
 * @defgroup zipsads
 * @ingroup zipsads
 */

drupal_add_css(drupal_get_path('module', 'zipsads') .'/zipsads.css');
require_once('zipsads.node.inc');

/**
 * Implementation of hook_block.
 */
function zipsads_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('Zipsads');
      return $blocks;
    case 'view':
      if (isset($_SESSION['zipsads_city'])) {
        $location = '<br>'. t('City:') . $_SESSION['zipsads_city'];
        $location .= '<br>'. t('State:') . $_SESSION['zipsads_state'];
        $location .= '<br>'. t('Country:') . $_SESSION['zipsads_country'];
        $location .= '<br>'. t('Zipcode:') . $_SESSION['zipsads_zipcode'];
      }
      else {
        $location = '<br>'. t('(No location selected)');
      }
      if (isset($_SESSION['zipsads_heading'])) {
        $category = '<br>'. t('Heading:') . $_SESSION['zipsads_heading'];
        $category .= '<br>'. t('Sub:') . $_SESSION['zipsads_subheading'];
      }
      else {
        $category = '<br>'. t('(No category selected)');
      }
      $items = array(
        l(t('Select location'), 'zipsads/select_location') . $location,
        l(t('Select category'), 'zipsads/select_category') . $category,
        l(t('View ad listings'), 'zipsads/view_ad_listings'),
        l(t('Create an ad'), 'node/add/zipsads-ad'),
        l(t('Manage my ads'), 'zipsads/manage_my_ads'),
//        l(t('Clear session variables'), 'zipsads/clear'),
//        l(t('Stuff database'), 'zipsads/stuff'), // cw
      );
      $block['subject'] = t('Zipsads');
      $block['content'] = theme('item_list', $items);
      return $block;
  }
}

/**
 * Implementation of hook_menu.
 */
function zipsads_menu() {
  $items[] = array('path' => 'zipsads/select_location',
    'title' => t('Select location'),
    'callback' => 'zipsads_select_location',
    'access' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items[] = array('path' => 'zipsads/select_location/list',
    'title' => t('List'),
    'access' => TRUE,
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -99,
  );
  $items[] = array('path' => 'zipsads/select_location/search',
    'title' => t('Search'),
    'callback' => 'drupal_get_form',
    'callback arguments' => array('zipsads_select_location_search_form'),
    'access' => TRUE,
    'type' => MENU_LOCAL_TASK,
    'weight' => -98,
  );
  $items[] = array('path' => 'zipsads/set_location',
    'title' => t('Set location'),
    'callback' => 'zipsads_set_location',
    'access' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items[] = array('path' => 'zipsads/select_category',
    'title' => t('Select category'),
    'callback' => 'zipsads_select_category',
    'access' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items[] = array('path' => 'zipsads/view_ad_listings',
    'title' => t('View ad listings'),
    'callback' => 'zipsads_view_ad_listings',
    'access' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items[] = array('path' => 'zipsads/view_ad_listings/list',
    'title' => t('List'),
    'access' => TRUE,
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -99,
  );
  $items[] = array('path' => 'zipsads/view_ad_listings/search',
    'title' => t('Search'),
    'callback' => 'drupal_get_form',
    'callback arguments' => array('zipsads_view_ad_listings_search_form'),
    'access' => TRUE,
    'type' => MENU_LOCAL_TASK,
    'weight' => -98,
  );
  $items[] = array('path' => 'zipsads/view_ad',
    'title' => t('View ad'),
    'callback' => 'zipsads_view_ad',
    'access' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items[] = array('path' => 'zipsads/manage_my_ads',
    'title' => t('Manage my ads'),
    'callback' => 'zipsads_manage_my_ads',
    'access' => user_access('edit own ad'),
    'type' => MENU_CALLBACK,
  );
  $items[] = array('path' => 'zipsads/renew_ad',
    'title' => t('Renew ad'),
    'callback' => 'zipsads_renew_ad',
    'access' => user_access('edit own ad'),
    'type' => MENU_CALLBACK,
  );

  $items[] = array('path' => 'zipsads/clear',
    'title' => t('Clear'),
    'callback' => 'zipsads_clear',
    'access' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items[] = array('path' => 'zipsads/stuff',
    'title' => t('Stuff'),
    'callback' => 'zipsads_stuff',
    'access' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_help.
 */
function zipsads_help($path) {
  switch ($path) {
    case 'admin/zipsads/locations/add':
      return '<p>'. t("") .'</p>';
      break;
    default:
      if (strstr($path, 'zipsads/view_ad_listings') && !strstr($path, 'search')) {
        return '<p>'. t('The default scope is <b>zipcode</b>. You can widen the scope by clicking either the <b>city</b>, <b>state</b>, or <b>country</b> link.') .'</p>';
      }
      break;
  }
}

/**
 * zipsads_select_location.
 */
function zipsads_select_location() {
  $content = '';
  $header = array(t('City'), t('State'), t('Country'), t('Zipcode'), t('Operations'));
  $rows = array();
  if (isset($_SESSION['zipsads_sql'])) {
    $sql = $_SESSION['zipsads_sql'];
    $result = db_query($sql);
    unset($_SESSION['zipsads_sql']);
  }
  else {
    $sql = "SELECT * FROM {zipcodes} ORDER BY `zip`";
    $result = pager_query($sql, 10);
  }
  while ($data = db_fetch_object($result)) {
    $rows[] = array($data->city, $data->state, $data->country, $data->zip, l(t('select'), 'zipsads/set_location/'. $data->city .'/'. $data->state .'/'. $data->country .'/'. $data->zip));
  }
  $content .= theme('table', $header, $rows);
  $content .= theme('pager', NULL, 10);
  return $content;
}

/**
 * zipsads_select_location_search_form.
 *
 * @ingroup forms
 * @see zipsads_select_location_search_form_submit()
 */
function zipsads_select_location_search_form() {
  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#description' => t('The city for this location search.'),
  );
  $form['search_city'] = array(
    '#type' => 'submit',
    '#value' => t('Search for city'),
  );
  $form['zipcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Zipcode'),
    '#description' => t('The zipcode for this location search.<br>NOTE: You must click the "Search for zipcode" button for this option.<br>Pressing the Enter key will not work.'),
  );
  $form['search_zipcode'] = array(
    '#type' => 'submit',
    '#value' => t('Search for zipcode'),
  );
  return $form;
}

/**
 * zipsads_select_location_search_form_submit.
 */
function zipsads_select_location_search_form_submit($form_id, $form_values) {
  $op = $form_values['op'];
  if ($op == 'Search for city') {
    $city = trim($form_values['city']);
    $_SESSION['zipsads_sql'] = sprintf("SELECT * FROM {zipcodes} WHERE `city` = '%s' ORDER BY `zip`", $city);
  }
  else if ($op == 'Search for zipcode') {
    $zipcode = trim($form_values['zipcode']);
    $_SESSION['zipsads_sql'] = sprintf("SELECT * FROM {zipcodes} WHERE `zip` = '%s' ORDER BY `zip`", $zipcode);
  }
  drupal_goto('zipsads/select_location');
}

/**
 * zipsads_set_location.
 */
function zipsads_set_location($city = NULL, $state = NULL, $country = NULL, $zipcode = NULL) {
  $_SESSION['zipsads_city'] = $city;
  $_SESSION['zipsads_state'] = $state;
  $_SESSION['zipsads_country'] = $country;
  $_SESSION['zipsads_zipcode'] = $zipcode;
  drupal_set_message('Location has been set.');
  drupal_goto('zipsads/select_category');
}

/**
 * zipsads_select_category.
 */
function zipsads_select_category() {
  if (!isset($_SESSION['zipsads_city'])) {
    drupal_set_message('You must select a location.', 'error');
    drupal_goto('zipsads/select_location');
  }
  $first = TRUE;
  $content = '';
  $heading = '';
  $subheadings = '';
  $rows = array();
  $header = array(t('Heading'), t('Subheadings'));
  $vid = variable_get('zipsads_categories_vocabulary', '');
  $sql = sprintf("SELECT * FROM {term_data} WHERE `vid` = %d ORDER BY `tid`", $vid);
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $parent = db_result(db_query("SELECT `parent` FROM {term_hierarchy} WHERE `tid` = %d", $data->tid));
    if ($parent == 0) {
      if ($first) {
        $first = FALSE;
      }
      else {
        $rows[] = array($heading, $subheadings);
      }
      $heading = $data->name;
      $subheadings = '';
    }
    else {
      $subheadings .= l($data->name, 'zipsads/view_ad_listings/'. $heading .'/'. $data->name .'/zipcode') .' | ';
    }
  }
  $rows[] = array($heading, $subheadings);
  return theme('table', $header, $rows);
}

/**
 * zipsads_view_ad_listings.
 */
function zipsads_view_ad_listings($heading = NULL, $subheading = NULL, $scope = NULL) {
  if (isset($_SESSION['zipsads_city'])) {
    $city = $_SESSION['zipsads_city'];
    $state = $_SESSION['zipsads_state'];
    $country = $_SESSION['zipsads_country'];
    $zipcode = $_SESSION['zipsads_zipcode'];
  }
  else {
    drupal_set_message('You must select a location.', 'error');
    drupal_goto('zipsads/select_location');
  }

  if ($heading) {
    $_SESSION['zipsads_heading'] = $heading;
    $_SESSION['zipsads_subheading'] = $subheading;
  }
  else if (isset($_SESSION['zipsads_heading'])) {
    $heading = $_SESSION['zipsads_heading'];
    $subheading = $_SESSION['zipsads_subheading'];
    $scope = 'zipcode';
  }
  else {
    drupal_set_message('You must select a category.', 'error');
    drupal_goto('zipsads/select_category');
  }

  if ($scope) {
    $_SESSION['zipsads_scope'] = $scope;
  }
  else {
    $scope = 'zipcode';
  }

  $content = '';

  $links = array();
  $links['title'] = array(
    'title' => 'Set scope to:',
  );
  $links['zipcode'] = array(
    'title' => 'zipcode',
    'href' => 'zipsads/view_ad_listings/'. $heading .'/'. $subheading .'/zipcode',
  );
  $links['city'] = array(
    'title' => 'city',
    'href' => 'zipsads/view_ad_listings/'. $heading .'/'. $subheading .'/city',
  );
  $links['state'] = array(
    'title' => 'state',
    'href' => 'zipsads/view_ad_listings/'. $heading .'/'. $subheading .'/state',
  );
  $links['country'] = array(
    'title' => 'country',
    'href' => 'zipsads/view_ad_listings/'. $heading .'/'. $subheading .'/country',
  );
  $content .= theme('links', $links);

  $header = array(t('Created'), t('Title'));

  $rows = array();

  switch ($scope) {
    case 'zipcode':
      $sql = sprintf("SELECT * FROM {zipsads_ad} WHERE `heading` = '%s' AND `subheading` = '%s' AND `city` = '%s' AND `state` = '%s' AND `country` = '%s' AND `zipcode` = '%s' ORDER BY `created` DESC",
        $heading, $subheading, $city, $state, $country, $zipcode);
      break;
    case 'city':
      $sql = sprintf("SELECT * FROM {zipsads_ad} WHERE `heading` = '%s' AND `subheading` = '%s' AND `city` = '%s' AND `state` = '%s' AND `country` = '%s' ORDER BY `created` DESC",
        $heading, $subheading, $city, $state, $country);
      break;
    case 'state':
      $sql = sprintf("SELECT * FROM {zipsads_ad} WHERE `heading` = '%s' AND `subheading` = '%s' AND `state` = '%s' AND `country` = '%s' ORDER BY `created` DESC",
        $heading, $subheading, $state, $country);
      break;
    case 'country':
      $sql = sprintf("SELECT * FROM {zipsads_ad} WHERE `heading` = '%s' AND `subheading` = '%s' AND `country` = '%s' ORDER BY `created` DESC",
        $heading, $subheading, $country);
      break;
  }
  if (isset($_SESSION['zipsads_title_keyword'])) {
    $result = db_query($sql);
    while ($data = db_fetch_object($result)) {
      $nsql = sprintf("SELECT `title` FROM {node_revisions} WHERE `nid` = %d", $data->nid);
      $nresult = db_query($nsql);
      $ndata = db_fetch_object($nresult);
      if (strstr($ndata->title, $_SESSION['zipsads_title_keyword'])) {
        $created = format_date($data->created);
        $rows[] = array($created, l($ndata->title, 'zipsads/view_ad/'. $data->nid));
      }
    }
    unset($_SESSION['zipsads_title_keyword']);
  }
  else if (isset($_SESSION['zipsads_body_keyword'])) {
    $result = db_query($sql);
    while ($data = db_fetch_object($result)) {
      $nsql = sprintf("SELECT `title`, `body` FROM {node_revisions} WHERE `nid` = %d", $data->nid);
      $nresult = db_query($nsql);
      $ndata = db_fetch_object($nresult);
      if (strstr($ndata->body, $_SESSION['zipsads_body_keyword'])) {
        $created = format_date($data->created);
        $rows[] = array($created, l($ndata->title, 'zipsads/view_ad/'. $data->nid));
      }
    }
    unset($_SESSION['zipsads_body_keyword']);
  }
  else {
    $result = pager_query($sql, 100);
    while ($data = db_fetch_object($result)) {
      $nsql = sprintf("SELECT * FROM {node_revisions} WHERE `nid` = %d", $data->nid);
      $nresult = db_query($nsql);
      $ndata = db_fetch_object($nresult);
      $created = format_date($data->created);
      $rows[] = array($created, l($ndata->title, 'zipsads/view_ad/'. $data->nid));
    }
  }
  $content .= theme('table', $header, $rows);
  $content .= theme('pager', NULL, 100);

  return $content;
}

/**
 * zipsads_view_ad_listings_search_form.
 *
 * @ingroup forms
 * @see zipsads_view_ad_listings_search_form_submit()
 */
function zipsads_view_ad_listings_search_form() {
  $form['title_keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('Title keyword'),
    '#description' => t('Enter a keyword to search for in title.'),
  );
  $form['search_title_keyword'] = array(
    '#type' => 'submit',
    '#value' => t('Search for keyword in title'),
  );
  $form['body_keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('Body keyword'),
    '#description' => t('Enter a keyword to search for in body.<br>NOTE: You must click the "Search for keyword in body" button for this option.<br>Pressing the Enter key will not work.'),
  );
  $form['search_body_keyword'] = array(
    '#type' => 'submit',
    '#value' => t('Search for keyword in body'),
  );
  return $form;
}

/**
 * zipsads_select_location_search_form_submit.
 */
function zipsads_view_ad_listings_search_form_submit($form_id, $form_values) {
  $op = $form_values['op'];
  if ($op == 'Search for keyword in title') {
    $title_keyword = trim($form_values['title_keyword']);
    $_SESSION['zipsads_title_keyword'] = $title_keyword;
  }
  else if ($op == 'Search for keyword in body') {
    $body_keyword = trim($form_values['body_keyword']);
    $_SESSION['zipsads_body_keyword'] = $body_keyword;
  }
  drupal_goto('zipsads/view_ad_listings/'. $_SESSION['zipsads_heading'] .'/'. $_SESSION['zipsads_subheading'] .'/'. $_SESSION['zipsads_scope']);
}

/**
 * zipsads_view_ad.
 */
function zipsads_view_ad($nid = NULL) {
  return theme('zipsads_ad', $nid);
}

/**
 * zipsads_theme_ad.
 */
function theme_zipsads_ad($nid) {
  global $user;
  $sql = sprintf("SELECT * FROM {node_revisions} WHERE `nid` = %d", $nid);
  $result = db_query($sql);
  $data = db_fetch_object($result);
  $zsql = sprintf("SELECT * FROM {zipsads_ad} WHERE `nid` = %d", $nid);
  $zresult = db_query($zsql);
  $zdata = db_fetch_object($zresult);
  $content = '';
  $content .= '<br><div><b>City:</b> '. $zdata->city .' <b>State:</b> '. $zdata->state .' <b>Country:</b> '. $zdata->country .' <b>Zipcode:</b> '. $zdata->zipcode .'</div>';
  $content .= '<br><div><b>Heading:</b> '. $zdata->heading .' <b>Subheading:</b> '. $zdata->subheading .'</div>';
  $content .= '<br><div><b>Contact:</b> '. l($user->mail, 'mailto:'. $user->mail) .'</div>';
  $content .= '<br><div><b>Title:</b> '. $data->title .'</div>';
  $content .= '<br><div><b>Body:</b> '. $data->body .'</div>';
  $content .= '<br><div>';
  $sql = sprintf("SELECT * FROM {files} WHERE `nid` = %d", $nid);
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $content .= theme('image', $data->filepath, '', 'title', array(), TRUE);
  }
  $content .= '</div>';
  return $content;
}

/**
 * zipsads_manage_my_ads.
 */
function zipsads_manage_my_ads() {
  global $user;
  $rows = array();
  $header = array(t('Created'), t('Expires'), t('Title'), array('data' => t('Operations'), 'colspan' => 3));
  $sql = sprintf("SELECT * FROM {node} WHERE `type` = 'zipsads_ad' AND `uid` = %d ORDER BY `created` DESC", $user->uid);
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $zsql = sprintf("SELECT * FROM {zipsads_ad} WHERE `nid` = %d", $data->nid);
    $zresult = db_query($zsql);
    $zdata = db_fetch_object($zresult);
    $created = explode(' ', format_date($zdata->created, 'small'));
    $expires = explode(' ', format_date($zdata->expires, 'small'));
    $rows[] = array($created[0], $expires[0], $data->title, l(t('edit'), 'node/'. $data->nid .'/edit'), l(t('delete'), 'node/'. $data->nid .'/delete'), l(t('renew'), 'zipsads/renew_ad/'. $data->nid));
  }
  return theme('table', $header, $rows);
}

/**
 * zipsads_renew_ad.
 */
function zipsads_renew_ad($nid = NULL) {
  $today = time();
  $sql = sprintf("UPDATE {node} SET `created` = %d, `changed` = %d WHERE `nid` = %d", $today, $today, $nid);
  $result = db_query($sql);
  $sql = sprintf("UPDATE {node_revisions} SET `timestamp` = %d WHERE `nid` = %d", $today, $nid);
  $result = db_query($sql);
  $sql = sprintf("UPDATE {zipsads_ad} SET `created` = %d, `expires` = %d, `notification_sent` = 0 WHERE `nid` = %d", $today, $today + 86400 * 30, $nid); // cw
  $result = db_query($sql);
  if ($result) {
    drupal_set_message('Your ad has been renewed.');
  }
  else {
    drupal_set_message('Your ad could not be renewed.', 'error');
  }
  drupal_goto('zipsads/manage_my_ads');
}

/**
 * zipsads_clear.
 */
function zipsads_clear() {
  unset($_SESSION['zipsads_city']);
  unset($_SESSION['zipsads_state']);
  unset($_SESSION['zipsads_country']);
  unset($_SESSION['zipsads_zipcode']);
  unset($_SESSION['zipsads_heading']);
  unset($_SESSION['zipsads_subheading']);
  drupal_goto('zipsads/manage_my_ads');
}

/**
 * zipsads_prep_node.
 */
function zipsads_prep_node($city, $state, $country, $zipcode, $heading, $subheading, $title, $body) {
  $node = new stdClass();
  $node->type = 'zipsads_ad';
  $node->uid = 2; // cw
  $node->status = 1;
  $node->promote = 0;
  $node->sticky = 0;
  $_SESSION['zipsads_city'] = $city;
  $_SESSION['zipsads_state'] = $state;
  $_SESSION['zipsads_country'] = $country;
  $_SESSION['zipsads_zipcode'] = $zipcode;
  $_SESSION['zipsads_heading'] = $heading;
  $_SESSION['zipsads_subheading'] = $subheading;
  $node->title = $title;
  $node->body = $body;
  return $node;
}

/**
 * zipsads_stuff.
 */
function zipsads_stuff() {
  $sql = "Select * FROM {zipsads_ad}";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    db_query("DELETE FROM {node} WHERE `nid` = %d", $data->nid);
    db_query("DELETE FROM {node_revisions} WHERE `nid` = %d", $data->nid);
  }
  db_query("DELETE FROM {zipsads_ad}");
  $node = zipsads_prep_node('Las Vegas', 'NV', 'us', '89123', 'for sale', 'boats', 'Great boat', 'Comes with trolling motor'); node_save($node);
  db_query("UPDATE {node_revisions} SET `uid` = 2 WHERE `nid` = %d", $node->nid);
  db_query("UPDATE {zipsads_ad} SET `uid` = 2 WHERE `nid` = %d", $node->nid);
  $node = zipsads_prep_node('Las Vegas', 'NV', 'us', '89120', 'for sale', 'boats', 'Wrecked boat', 'Comes with spare parts'); node_save($node);
  db_query("UPDATE {node_revisions} SET `uid` = 2 WHERE `nid` = %d", $node->nid);
  db_query("UPDATE {zipsads_ad} SET `uid` = 2 WHERE `nid` = %d", $node->nid);
  $node = zipsads_prep_node('Reno', 'NV', 'us', '89501', 'for sale', 'boats', 'Tug boat', 'Tug it to where you want it.'); node_save($node);
  db_query("UPDATE {node_revisions} SET `uid` = 2 WHERE `nid` = %d", $node->nid);
  db_query("UPDATE {zipsads_ad} SET `uid` = 2 WHERE `nid` = %d", $node->nid);
  $node = zipsads_prep_node('Marina Del Rey', 'CA', 'us', '90292', 'for sale', 'boats', 'One man\'s yacht', 'Is another man\'s weekend project.'); node_save($node);
  db_query("UPDATE {node_revisions} SET `uid` = 2 WHERE `nid` = %d", $node->nid);
  db_query("UPDATE {zipsads_ad} SET `uid` = 2 WHERE `nid` = %d", $node->nid);
  drupal_goto('zipsads/manage_my_ads');
}

/**
 * Implementation of hook_cron.
 */
function zipsads_cron() {
  global $base_url;
  $today = time();
  $sql = "SELECT * FROM {zipsads_ad}";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    if ($today > $data->expires) {
      db_query("DELETE FROM {node} WHERE `nid` = %d", $data->nid);
      db_query("DELETE FROM {node_revisions} WHERE `nid` = %d", $data->nid);
      $fsql = sprintf("SELECT * FROM {files} WHERE `nid` = %d", $data->nid);
      $fresult = db_query($fsql);
      while ($fdata = db_fetch_object($fresult)) {
        $command = sprintf("unlink %s", $fdata->filepath);
        system($command);
      }
      db_query("DELETE FROM {files} WHERE `nid` = %d", $data->nid);
    }
    else if ($today > ($data->expires - 86400 * 7)) {
      $sent = db_result(db_query("SELECT `notification_sent` FROM {zipsads_ad} WHERE `nid` = %d", $data->notification_sent));
      if ($sent == 0) {
        $from = db_result(db_query("SELECT `mail` FROM {users} WHERE `uid` = 1"));
        $to = db_result(db_query("SELECT `mail` FROM {users} WHERE `uid` = %d", $data->uid));
        $subject = 'Your classified ad is about to expire.';
        $body = 'Please go to '. $base_url .' if you would like to renew it.';
        $headers = array(
          "From" => $from,
          "Reply-to" => $from,
          "X-Mailer" => "PHP",
          "Return-path" => $from,
          "Errors-to" => $from,
        );
        drupal_mail('zipsads_notification', $to, $subject, $body, NULL, $headers);
        db_query("UPDATE {zipsads_ad} SET `notification_sent` = 1 WHERE `nid` = %d", $data->nid);
      }
    }
    $fsql = sprintf("SELECT count(*) as count FROM {files} WHERE `nid` = %d", $data->nid);
    $count = db_result(db_query($fsql));
    if ($count > 4) {
      $fsql = sprintf("SELECT * FROM {files} WHERE `nid` = %d", $data->nid);
      $fresult = db_query($fsql);
      while ($fdata = db_fetch_object($fresult)) {
        $command = sprintf("unlink %s", $fdata->filepath);
        system($command);
      }
      db_query("DELETE FROM {files} WHERE `nid` = %d", $data->nid);
    }
  }
}

