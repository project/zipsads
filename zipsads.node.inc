<?php

/**
 * @file
 *   Node functions for the zipsads module.
 * @ingroup zipsads
 */

/**
 * Implementation of hook_node_info.
 */
function zipsads_node_info() {
  return array(
    'zipsads_ad' => array(
      'name' => t('Zipsads ad'),
      'module' => 'zipsads',
      'description' => t('Zipsads ad'),
      'has_title' => TRUE,
      'title_label' => t('Zipsads ad title'),
      'has_body' => TRUE,
      'body_label' => t('Zipsads ad body'),
      'min_word_count' => 3,
    )
  );
}

/**
 * Implementation of hook_perm.
 */
function zipsads_perm() {
  return array('create ad', 'edit own ad');
}

/**
 * Implementation of hook_access.
 */
function zipsads_access($op, $node) {
  global $user;
  if ($op == 'create') {
    return user_access('create ad');
  }
  if ($op == 'update' || $op == 'delete') {
    return (user_access('edit own ad') && ($user->uid == $node->uid));
  }
}

/**
 * Implementation of hook_form.
 */
function zipsads_form($node) {
  if (!isset($_SESSION['zipsads_city'])) {
    drupal_set_message('You must select a location.', 'error');
    drupal_goto('zipsads/select_location');
  }
  if (!isset($_SESSION['zipsads_heading'])) {
    drupal_set_message('You must select a category.', 'error');
    drupal_goto('zipsads/select_category');
  }
  $type = node_get_types('type', $node);
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#description' => t('The title may not include angle brackets.'),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5,
  );
  $max_body_length = 500;
  $current_body_length = strlen($node->body);
  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => check_plain($type->body_label),
    '#description' => t('The body may include a maximum of 500 characters.'),
    '#default_value' => $node->body,
    '#required' => TRUE,
    '#attributes' => array('onkeyup' => "document.getElementById('body_length_remaining').innerHTML = this.value.length; document.getElementById('body_length_remaining').setAttribute('class', this.value.length <= $max_body_length ? 'classified-bodylength-ok' : 'classified-bodylength-exceeded');"),
  );
  $form['body_filter']['body_length_remaining'] = array(
    '#type' => 'markup', 
    '#value' => t('Body characters used: <span id="body_length_remaining">%initial_value</span> of %max_value',
      array('%initial_value' => $current_body_length, '%max_value' => $max_body_length)
    ), 
    '#weight' => -1,
    '#prefix' => '<span id="classified-bodylength-msg">',
    '#suffix' => '</span>',
  );
  $form['body_filter']['filter'] = filter_form($node->format);
  return $form;
}

function zipsads_validate(&$node) {
  if (strstr($node->title, '<') || strstr($node->title, '>')) {
    form_set_error('title', t('The title may not contain angle brackets.'));
  }
  if (strlen($node->body) > 500) {
    form_set_error('body', t('The maximum body length has been exceeded.'));
  }
}

/**
 * Implementation of hook_form_validate.
 */
function zipsads_form_validate($form_id, $form_values) {
  $body = $form_values['body'];
$msg = sprintf("%d", strlen($body));
drupal_set_message($msg);
}

/**
 * Implementation of hook_insert.
 */
function zipsads_insert($node) {
  global $user;
  $expires = $node->created + 86400 * 30;
  $sql = sprintf("INSERT INTO {zipsads_ad} (`nid`, `vid`, `uid`, `created`, `expires`, `heading`, `subheading`, `city`, `state`, `country`, `zipcode`) VALUES (%d, %d, %d, %d, %d, '%s', '%s', '%s', '%s', '%s', '%s')",
    $node->nid, $node->vid, $user->uid, $node->created, $expires, $_SESSION['zipsads_heading'], $_SESSION['zipsads_subheading'], $_SESSION['zipsads_city'], $_SESSION['zipsads_state'], $_SESSION['zipsads_country'], $_SESSION['zipsads_zipcode']);
  db_query($sql);
}

/**
 * Implementation of hook_update.
 */
function zipsads_update($node) {
  $sql = sprintf("UPDATE {zipsads_ad} SET `vid` = %d WHERE `nid` = %d", $node->vid, $node->nid);
  db_query($sql);
}

/**
 * Implementation of hook_delete.
 */
function zipsads_delete($node) {
  $sql = sprintf("DELETE FROM {zipsads_ad} WHERE `nid` = %d", $node->nid);
  db_query($sql);
}

