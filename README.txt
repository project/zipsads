Zipsads
(a zip code driven classified ads website module)

by Carl A. Wenrich


Table of Contents
Introduction	1
The craigslist problem	1
The zipsads approach	1
Installation	2
The location module	2
The zipsads module	2
Browsing ads	2
Selecting the location	2
Selecting a category	2
Viewing ad listings	2
Widening the scope	2
Searching ad listings	3
Placing an ad	3
Managing ads	3

Introduction

The craigslist problem

Craigslist is the de facto standard for classified ads websites. But it has a problem. Although it portrays itself as a local service, it winds up being local to only a few metropolitan areas. Of the thousands of localities in the United States, it serves only 449.
For example, if I want to advertise in Nevada I have only two choices, Las Vegas and Reno. But what if I live in Ely, which is hundreds of miles from either one of those? The fact is that craigslist is only local  for you if you (and your potential customer) live in one of its served cities.

The zipsads approach

Zipsads takes a different approach. There are 43,187 zip codes in the United States. Zipsads simply makes each one of these a locality. When you want to place an ad, you select a location (city, state, country, zip code) and a category (heading and subheading) and create the ad.
This naturally places a limit on the coverage a given ad will enjoy. Zipsads solves this problem by allowing the user to adjust the scope. A person browsing the ads in a certain category can select to views ads (1) within his zip code, (2) within his city, (3) within his state, or (4) nationwide.

Installation

The creators of the location module have already gathered the 43,187 United States zip codes and their associated city, state, and country data. Rather than wasting time reinventing the wheel, we will simply download and install this module, and then import the zipcodes table.

The location module

The location module is installed in the sites/all/modules folder using the standard procedure for installing contributed modules. The process creates the zipcodes table but doesn't populate it. To place the 43,187 rows into the table we need to use the phpmyadmin utility, and import the sites/all/modules/location/database/zipcodes.us.mysql file.

The zipsads module

The zipsads module is installed in the sites/all/modules folder using the standard procedure for installing contributed modules. The Zipsads block is then placed in the left column.

Browsing ads

Anyone can browse the ads on a zipsads site. There is no need to register or log in.

Selecting the location

A user selects the location by clicking the 'Zipsads' > 'Select location' link. A list of 43,187 rows appears on pages of 10 at a time. A 'Search' feature makes this manageable.
When he clicks the 'Search' link a page appears allowing him to enter either a city or a zip code to bring the number of selections available down to something reasonable.
After he selects a location, the city, state, country, and zip code appear in the Zipsads block.

Selecting a category

A user is taken to the category selection page after he selects a location. If he wishes to change the category later on he can click the 'Zipsads' > 'Select category' link.
After he selects a category, the heading and subheading appear in the Zipsads block.

Viewing ad listings

A user is taken to the ad listings page after he selects a category. If he wishes to return to it later on he can click the 'Zipsads' > 'View ad listings' link.

Widening the scope

The listings for a given location within a given category are displayed on pages of 100 at a time. The  user can click the 'zipcode', 'city', 'state', or 'country' link to widen or narrow the scope to view all the ads placed  (1) within his zip code, (2) within his city, (3) within his state, or (4) nationwide.

Searching ad listings

A user can restrict the number of listings displayed by searching for a keyword in either the ad titles or the ad bodies. The listings displayed will then be filtered by both the search keyword and the scope. The scope should be set first, as the search keyword is cleared immediately after use.

Placing an ad

To place an ad, a user must first register and log in. He must then select a location and a category. Then he can click the 'Zipsads' > 'Create an ad' link to place an ad. The location and category information are stored as part of the ad.
He enters a title and a body (500 character max) and then has the option of uploading up to 4 images. Upon completion his ad is immediately posted with a 30-day expiration.

Managing ads

A user can edit, delete, or renew ads by clicking the 'Zipsads' > 'Manage my ads' link. He can change the title and body of an ad, or change the images included. When he renews an ad, the 'created' date is updated to the current date, and the 'expires' date to a date 30 days out.
